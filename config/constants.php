<?php

return array(
    'uploads_dir'        => '/uploads/',
    'archive__dir'       => '/uploads/archives/',
    'media_tags'         => ['gallery'],
    'paginate'           => '10',
    'image_per_folder'   => '10000',
    'excelimporter'      => 'excelimporter',
    'gd_supported_files' => ["jpg", "jpeg", "png", "webp", "gif"],
);
