<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldRemark extends Model
{
    protected $fillable = ['old_id','remark','user_name'];
}
