<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SEOAnalytics extends Model
{
    protected $table = 'seo_analytics';
}
