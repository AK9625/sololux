<?php

namespace App\Http\Controllers;

use App\Compositions;
use Illuminate\Http\Request;

class CompositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compositions  $compositions
     * @return \Illuminate\Http\Response
     */
    public function show(Compositions $compositions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compositions  $compositions
     * @return \Illuminate\Http\Response
     */
    public function edit(Compositions $compositions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compositions  $compositions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compositions $compositions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compositions  $compositions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compositions $compositions)
    {
        //
    }
}
