<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogMagento extends Model
{
    protected $table = 'log_magento';
    public $timestamps = false;
}
